
package org.jboss.tools.controller;

import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.tools.model.Restaurant;
import org.jboss.tools.service.RestaurantRegistration;

@ManagedBean
@Model
public class RestaurantController {

	@Inject
	private Logger log;

	@Inject
	private FacesContext facesContext;

	@Inject
	private RestaurantRegistration restaurantRegistration;

	@Produces
	@Named
	private Restaurant newRestaurant;

	@PostConstruct
	public void initNewRestaurant() {
		newRestaurant = new Restaurant();

	}

	public String register() throws Exception {
		try {
			restaurantRegistration.register(newRestaurant);
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Restaurant Registered!",
					"Restaurant Registration successful");
			facesContext.addMessage(null, m);
			initNewRestaurant();
			return "election";
		} catch (Exception e) {
			String errorMessage = getRootErrorMessage(e);
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage,
					"Restaurant Registration unsuccessful");
			facesContext.addMessage(null, m);
			return "restaurant";
		}
	}

	private String getRootErrorMessage(Exception e) {
		// Default to general error message that registration failed.
		String errorMessage = "Registration failed. See server log for more information";
		if (e == null) {
			// This shouldn't happen, but return the default messages
			return errorMessage;
		}

		// Start with the exception and recurse to find the root cause
		Throwable t = e;
		while (t != null) {
			// Get the message from the Throwable class instance
			errorMessage = t.getLocalizedMessage();
			t = t.getCause();
		}
		// This is the root cause message
		return errorMessage;
	}

}

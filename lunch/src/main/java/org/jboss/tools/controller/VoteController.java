package org.jboss.tools.controller;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpSession;

import org.jboss.tools.data.ElectionRepository;
import org.jboss.tools.data.UserRepository;
import org.jboss.tools.data.VoteRepository;
import org.jboss.tools.model.Vote;
import org.jboss.tools.model.Election;
import org.jboss.tools.model.Restaurant;
import org.jboss.tools.service.VoteRegistration;
import org.joda.time.DateTime;
import org.joda.time.Days;

@ManagedBean(eager = true)
@Model
@SessionScoped
public class VoteController {

	@Inject
	private Logger log;

	private String activeUsera; 

	@Inject
	private FacesContext facesContext;

	@Inject
	private VoteRegistration voteRegistration;

	@Produces
	@Named
	private Vote newVote;
	@Named
	private Map<Integer, Integer> voteMapView;

	@PostConstruct
	public void initNewVote() {
		newVote = new Vote();
		voteMapView = new HashMap<Integer, Integer>();
	}

	@Inject
	private VoteRepository voteRepository;

	@Inject
	private UserRepository userRepository;

	@Inject
	private ElectionRepository electionRepository;

	private Election activeElection;

	public Map<Integer, Integer> getVoteMap() {

		return voteMapView;
	}

	public String register(Restaurant voted) throws Exception {
		try {
			// Get Active Election
			activeElection = electionRepository.findActiveElectionOrNull();

			if (activeElection != null) {

				// Get user Logged in information
				log.info("Getting user information from HTTP Session ");				
				HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
				String usrName = session.getAttribute("username").toString();
				Long usrId = userRepository.findByName(usrName).getId();
				boolean canVoteForRestaurant = false;
				// Check if user voted already
				if (voteRepository.userCanVote(usrId)) {

					// Check if restaurant can be voted(more than one week since
					// last visit)
					if (voted.getLastVisit() != null) {
						DateTime dateTimeLastVisit = new DateTime(voted.getLastVisit());
						DateTime dateTimeElectionDate = new DateTime(activeElection.getDate());
						int interval = Days.daysBetween(dateTimeLastVisit, dateTimeElectionDate).getDays();
						if (interval < 7) {
							log.info("Vote Invalid, Restaurant recentely visited! ");
							

							FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR,
									"Vote Invalid, Restaurant recentely visited!", "Restaurant recentely visited!");
							facesContext.addMessage(null, m);
							return "result";
						} else {
							canVoteForRestaurant = true;
						}

					}
					if ((voted.getLastVisit() == null) || (canVoteForRestaurant)) {

						// Register Vote
						newVote.setElectionId(activeElection.getId());
						newVote.setRestaurantId(voted.getId());
						newVote.setUserId(usrId);
						voteRegistration.register(newVote);

						FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Vote Recorded!",
								"Vote Has been Registered!");
						facesContext.addMessage(null, m);
						initNewVote();

						return "result";
					}
				} else {
					FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR,
							"User already casted a vote in this election",
							"User already casted a vote in this election");
					facesContext.addMessage(null, m);
					return "result";
				}
			}
		} catch (Exception e) {
			String errorMessage = getRootErrorMessage(e);
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "User Cannot Vote",
					"Unable to Reccord User Vote");
			facesContext.addMessage(null, m);
		}
		return "result";

	}

	private String getRootErrorMessage(Exception e) {
		// Default to general error message that registration failed.
		String errorMessage = "Registration failed. See server log for more information";
		if (e == null) {
			// This shouldn't happen, but return the default messages
			return errorMessage;
		}

		// Start with the exception and recurse to find the root cause
		Throwable t = e;
		while (t != null) {
			// Get the message from the Throwable class instance
			errorMessage = t.getLocalizedMessage();
			t = t.getCause();
		}
		// This is the root cause message
		return errorMessage;
	}

}

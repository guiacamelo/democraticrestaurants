package org.jboss.tools.controller;

import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import javax.servlet.http.HttpSession;
import org.jboss.tools.data.RestaurantRepository;
import org.jboss.tools.data.UserRepository;
import org.jboss.tools.model.Restaurant;
import org.jboss.tools.model.User;
import org.jboss.tools.service.UserRegistration;
import org.jboss.tools.util.SessionUtils;

@ManagedBean(eager = true)
@Model
@SessionScoped
public class UserController {

	@Inject
	private Logger log;

	@Inject
	private FacesContext facesContext;

	@Inject
	private UserRegistration userRegistration;

	@Produces
	@Named
	private User newUser;

	private String activeUserName;
	private Long activeUserId;

	public String getActiveUserName() {
		return activeUserName;
	}

	public Long getActiveUserId() {
		return activeUserId;
	}

	List<Restaurant> listR;

	@PostConstruct
	public void initNewUser() {
		newUser = new User();

	}
	public void print(){
		System.out.println("HII");
	}
	public String register() throws Exception {
		try {

			userRegistration.register(newUser);
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Registered!", "Registration successful");
			facesContext.addMessage(null, m);
			initNewUser();
			return "login";
		} catch (Exception e) {
			String errorMessage = getRootErrorMessage(e);
			FacesMessage m;
			m = new FacesMessage(FacesMessage.SEVERITY_ERROR, "Registration unsuccessful",
					"Check if user already exists, name have no numbers, a email is in correct format");
			facesContext.addMessage(null, m);
			return "register";
		}

	}

	@Inject
	private UserRepository repository;

	@Inject
	private RestaurantRepository repositoryR;

	public String getActiveUsera() throws Exception {
		HttpSession session = SessionUtils.getSession();
		return session.getAttribute("username").toString();

	}

	public String login() throws Exception {
		boolean valid = repository.validate(newUser.getName(), newUser.getPassword());
		if (valid) {
			log.info("Validation HTTP Session and Setting User attributes");
			HttpSession session = SessionUtils.getSession();
			session.setAttribute("userid", newUser.getId());
			session.setAttribute("username", newUser.getName());
			activeUserName = newUser.getName();
			activeUserId = newUser.getId();
			return "election";
		} else {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
					"Incorrect Username and Passowrd", "Please enter correct username and Password"));
			return "login";

		}
	}

	// logout event, invalidate session
	public String logout() {
		HttpSession session = SessionUtils.getSession();
		session.invalidate();
		return "login";
	}



	private String getRootErrorMessage(Exception e) {
		// Default to general error message that registration failed.
		String errorMessage = "Registration failed. See server log for more information";
		if (e == null) {
			// This shouldn't happen, but return the default messages
			return errorMessage;
		}

		// Start with the exception and recurse to find the root cause
		Throwable t = e;
		while (t != null) {
			// Get the message from the Throwable class instance
			errorMessage = t.getLocalizedMessage();
			t = t.getCause();
		}
		// This is the root cause message
		return errorMessage;
	}

}

package org.jboss.tools.controller;

import java.io.Serializable; 

import javax.enterprise.context.RequestScoped;
import javax.faces.bean.ManagedBean;

@SuppressWarnings("serial")
@ManagedBean(name = "navigationController", eager = true)
@RequestScoped
public class NavigationController implements Serializable {

	public String moveToLoginPage() {

		return "login";
	}

	public String moveToRegisterPage() {
		return "register";
	}

	public String moveToRestaurantPage() {
		return "restaurant";
	}

	public String moveToVotePage() {
		return "election";
	}
}

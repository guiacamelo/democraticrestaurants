package org.jboss.tools.controller;

import java.text.ParseException;  
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Logger;

import javax.enterprise.inject.Model;
import javax.enterprise.inject.Produces;
import javax.faces.application.FacesMessage;

import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import org.jboss.tools.data.ElectionRepository;
import org.jboss.tools.data.RestaurantRepository;
import org.jboss.tools.data.UserRepository;
import org.jboss.tools.data.VoteRepository;
import org.jboss.tools.model.Election;
import org.jboss.tools.model.Restaurant;
import org.jboss.tools.model.User;
import org.jboss.tools.model.Vote;
import org.jboss.tools.service.ElectionRegistration;
import org.jboss.tools.service.RestaurantRegistration;
import org.jboss.tools.util.EmailUtils;

@Model
@SessionScoped
public class ElectionController {
	
	@Inject
	private Logger log;
	
	@Inject
	private FacesContext facesContext;

	@Inject
	private UserRepository userRepository;
	
	@Inject
	private ElectionRegistration electionRegistration;

	@Inject
	private RestaurantRepository restaurantRepository;

	@Inject
	private RestaurantRegistration restaurantRegistration;

	@Inject
	private VoteRepository voteRepository;

	private Vote newVote;

	@Produces
	@Named
	private Election newElection;

	@Named
	private Restaurant voteRest;

	@Inject
	private ElectionRepository electionRepository;

	private Election activeElection;
	@Named
	private Map<Integer, Integer> voteMapView;

	public Map<Integer, Integer> getVoteMap() {

		return voteMapView;
	}



	// Get current date
	public Date getCurrentDate() throws ParseException {
		Date currentDate;
		currentDate = Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant());
		return currentDate;
	}

	public void register() throws Exception {
		try {

			// Check if there is an active election
			activeElection = electionRepository.findActiveElectionOrNull();
			if (activeElection == null) {
				log.info("No Active Election Found " + activeElection);
				// This register a new election
				newElection = new Election();
				newElection.setActive(true);
				newElection.setDate(getCurrentDate());
				
				electionRegistration.register(newElection);
				FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "New Election Created", " ");
				facesContext.addMessage(null, m);
				activeElection = newElection;		


			} 
			log.info("Current Active Election is " + activeElection.getId());
		} catch (Exception e) {
			String errorMessage = getRootErrorMessage(e);
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, "Registration unsuccessful");
			facesContext.addMessage(null, m);
		}
	}

	private Map<Integer, Integer> createVoteMap() {
		Map<Integer, Integer> voteMap = new HashMap<Integer, Integer>();
		List<Restaurant> restaurantList;
		restaurantList = restaurantRepository.findAllOrderedByName();
		Iterator<Restaurant> restaurantIterator = restaurantList.iterator();
		while (restaurantIterator.hasNext()) {
			// Get restaurant Id and iterate to next restaurant
			Integer restid = (int) (long) restaurantIterator.next().getId();
			Integer qtyVotes = voteRepository.getVotesByRestaurantIdOfActiveElection(restid);
			String resName;
			voteMap.put(restid, qtyVotes);

			// Get name of restaurant to store on vote map view to be rendered
			resName = restaurantRepository.findById(restid.longValue()).getName();
			voteMapView.put(restid, qtyVotes);
		}
		//log.info("Creating Vote Map");
		
		return voteMap;
	}

	private String getRootErrorMessage(Exception e) {
		// Default to general error message that registration failed.
		String errorMessage = "Registration failed. See server log for more information";
		if (e == null) {
			// This shouldn't happen, but return the default messages
			return errorMessage;
		}

		// Start with the exception and recurse to find the root cause
		Throwable t = e;
		while (t != null) {
			// Get the message from the Throwable class instance
			errorMessage = t.getLocalizedMessage();
			t = t.getCause();
		}
		// This is the root cause message
		return errorMessage;
	}

	public long getWinnerId(Election election) {
		List<Long> winner = new ArrayList<Long>();
		try {
			winner = getWinners();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// Return Id of restaurant Winner
		return winner.get(0);
	}

	public String finishElection() throws Exception {
		newVote = new Vote();
		voteMapView = new HashMap<Integer, Integer>();
		createVoteMap();
		activeElection = electionRepository.findActiveElectionOrNull();
		
		
		if (activeElection != null) {
			log.info("Active Election found " );
			// Get election winner
			Long winnerId;
			String winnerName;
			List<Long> winnerIdList=getWinners();
			if (winnerIdList.size() == 1) {
				
				winnerId = getWinnerId(activeElection);
				winnerName = restaurantRepository.findById(winnerId).getName();
						
				log.info("Single Winner Found "+ winnerId+ " "+ winnerName);
				FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO,
						"Election Finished, The winner is " + winnerName,
						"Election Finished, The winner is " + winnerName);
				facesContext.addMessage(null, m);
				log.info("Finishing Election of Date " + activeElection.getDate());

				// Update Last Visit To the Restaurant
				restaurantRegistration.updateRestaurantVisit(winnerId, activeElection.getDate());

				
				// Update Election Winner and Setting election to not active
				electionRegistration.updateWinnerId(activeElection, winnerId);
				sendMail(winnerName,activeElection.getDate());
				
				
				
				
				
			} else if (winnerIdList.size() > 1) {
				log.info("Multiple Winners Found");
				
				// A tie
				winnerId = winnerIdList.get(0);
				winnerName = restaurantRepository.findById(winnerId).getName();
				log.info("The Arbitrary Winner is restaurant:"+winnerId);
				
				FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_INFO, "Election Finished",
						"A tie has been presented, the arbitrary chosen winner is "
								+ winnerName);
				facesContext.addMessage(null, m);

				// Update Last Visit To the Restaurant
				restaurantRegistration.updateRestaurantVisit(winnerId, activeElection.getDate());
				
				// Update Election Winner and deactivate election
				electionRegistration.updateWinnerId(activeElection, winnerId);

				//Send Mail
				sendMail(winnerName,activeElection.getDate());
			}
			return "login";
			

		}
		return "election";
	}

	private void sendMail(String winner, Date date) {
		String email;
		String name;
		User usr;
		List<User> userList;
		userList = userRepository.findAllOrderedByName();
		Iterator<User> userIterator = userList.iterator();
		while (userIterator.hasNext()) {
			// Get restaurant Id and iterate to next restaurant
			usr = userIterator.next();
			email = usr.getEmail();
			name = usr.getName();
			EmailUtils.sendMail(winner,name, email, date);
			log.info("Sending Mail to "+ name + " - "+ email);
		}
		
	}



	@SuppressWarnings("null")
	public List<Long> getWinners() throws Exception {
		try {
			Map<Integer, Integer> voteMap = new HashMap<Integer, Integer>();
			voteMap = createVoteMap();
			// Get max quantity of votes
			int maxValueInMap = (Collections.max(voteMap.values()));
			List<Long> mostVottedRestaurantIdList = new ArrayList<Long>();

			// Iterate has and find restaurants with max votes(check for ties)
			for (Entry<Integer, Integer> entry : voteMap.entrySet()) {

				if (entry.getValue() == maxValueInMap) {
					mostVottedRestaurantIdList.add(entry.getKey().longValue());
				}
			}
			return mostVottedRestaurantIdList;
		} catch (Exception e) {
			String errorMessage = getRootErrorMessage(e);
			FacesMessage m = new FacesMessage(FacesMessage.SEVERITY_ERROR, errorMessage, "Registration unsuccessful");
			facesContext.addMessage(null, m);
		}

		return null;
	}

}

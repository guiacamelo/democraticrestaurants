package org.jboss.tools.service;

import org.jboss.tools.model.User;

import javax.ejb.Stateless;
//import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;

import java.util.logging.Logger;

@Stateless
public class UserRegistration {

	@Inject
	private Logger log;

	@Inject
	private EntityManager em;

	public void register(User user) throws Exception {
		log.info("Registering User " + user.getName());
		
	
	
		em.persist(user);

		// userEventSrc.fire(user);
	}
	
}

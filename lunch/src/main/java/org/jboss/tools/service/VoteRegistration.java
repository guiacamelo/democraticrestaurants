package org.jboss.tools.service;

import org.jboss.tools.model.Vote;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
//import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import java.util.logging.Logger;

@Stateless
public class VoteRegistration {

	@Inject
	private Logger log;

	@Inject
	private EntityManager em;

	@Inject
	private Event<Vote> voteEventSrc;

	public void register(Vote vote) throws Exception {
		log.info("Registering Vote" + vote.getId());
		em.persist(vote);
		voteEventSrc.fire(vote);
	}
}

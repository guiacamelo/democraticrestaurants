package org.jboss.tools.service;

import org.jboss.tools.model.Election;  

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.util.logging.Logger;

@Stateless
public class ElectionRegistration {

	@Inject
	private Logger log;

	@Inject
	private EntityManager em;

	@Inject
	private Event<Election> electionEventSrc;

	public void register(Election election) throws Exception {
		log.info("Registering Election" + election.getId());
		em.persist(election);

		electionEventSrc.fire(election);
	}

	public void updateWinnerId(Election election, Long winnerId) {
		//This Function update the election winner and deactivate the election using Entity Manager merge
		if (em.find(Election.class, election.getId()) == null) {

			throw new IllegalArgumentException("Unknown Election id, Did not Update Winner");

		}
		election.setWinner(winnerId);
		election.setActive(false);

		this.em.merge(election);
		log.info("Updating election winner " + winnerId + " and deactivating election");

	}

	public void deactivateElection(Election election) {
		if (em.find(Election.class, election.getId()) == null) {
			throw new IllegalArgumentException("Unknown Election id, Did not deactivate election");
		}

		election.setActive(false);
		this.em.persist(election);
	}

}

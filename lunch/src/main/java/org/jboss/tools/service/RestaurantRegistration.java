package org.jboss.tools.service;

import org.jboss.tools.model.Restaurant;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import java.util.Date;
import java.util.logging.Logger;

@Stateless
public class RestaurantRegistration {

	@Inject
	private Logger log;

	@Inject
	private EntityManager em;

	@Inject
	private Event<Restaurant> restaurantEventSrc;

	public void register(Restaurant restaurant) throws Exception {
		log.info("Registering Restaurant " + restaurant.getName());
		em.persist(restaurant);
		restaurantEventSrc.fire(restaurant);
	}

	public void updateRestaurantVisit(long winnerId, Date lastVisitDate) {
		
	
		Restaurant restaurantUpdate = em.find(Restaurant.class, winnerId);
		if(restaurantUpdate ==null){
	           throw new IllegalArgumentException("Unknown Restaurant id, Did not Update Winner");
	       }	
		restaurantUpdate.setLastVisit(lastVisitDate);		
		this.em.persist(restaurantUpdate);
		log.info("Updating restaurant "+ winnerId+" last visit: "+ lastVisitDate.toString());

	}
}

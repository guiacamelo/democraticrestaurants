package org.jboss.tools.data;

import javax.ejb.SessionContext; 
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NonUniqueResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import java.util.Date;
import java.util.List;
import java.util.Map;
import org.jboss.tools.model.Election;

@ApplicationScoped
public class ElectionRepository {

	private SessionContext sessionContext;

	@Inject
	private EntityManager em;

	@Inject
	RestaurantRepository restaurantRepository;

	@Inject
	VoteRepository voteRepository;

	private Map<Integer, Integer> voteMapView;

	public Map<Integer, Integer> getvoteMapView() {
		return voteMapView;
	}

	public Election findById(Long id) {
		return em.find(Election.class, id);
	}

	public Election findByDate(Date voteDate) {

		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Election> criteria = cb.createQuery(Election.class);
		Root<Election> election = criteria.from(Election.class);
		criteria.select(election).where(cb.equal(election.get("voteDate"), voteDate));
		return em.createQuery(criteria).getSingleResult();
	}

	public Election findActiveElectionOrNull() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Election> criteria = cb.createQuery(Election.class);
		Root<Election> election = criteria.from(Election.class);
		criteria.select(election).where(cb.equal(election.get("active"), true));
		List<Election> resultList;
		resultList = em.createQuery(criteria).getResultList();

		if (resultList.isEmpty())
			return null;
		else if (resultList.size() == 1)
			return resultList.get(0);
		throw new NonUniqueResultException();
	}

	public List<Election> findAllOrderedByWinner() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Election> criteria = cb.createQuery(Election.class);
		Root<Election> election = criteria.from(Election.class);
		criteria.select(election).orderBy(cb.asc(election.get("winner")));
		return em.createQuery(criteria).getResultList();
	}

//	public boolean singleWinner() throws Exception {
//		List<Long> winners = new ArrayList<Long>();
//		winners = getWinners();
//
//		if (winners.size() == 1)
//			return true;
//		else
//			return false;
//	}
//
//	public long getWinnerId(Election election) {
//		List<Long> winner = new ArrayList<Long>();
//		try {
//			winner = getWinners();
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		// Return Id of restaurant Winner
//		return winner.get(0);
//	}
//
//	public List<Long> getTiedWinnerId(List<Long> winnerIdList, Election election) {
//		List<Long> winner = new ArrayList<Long>();
//		try {
//			winner = getWinners();
//
//		} catch (Exception e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		// Return Id of restaurant Winner
//		return winner;
//	}
//
//	private Map<Integer, Integer> createVoteMap() {
//		// Create hashmap of all votes < restaurantId,quantity>
//		Map<Integer, Integer> voteMap = new HashMap<Integer, Integer>();
//		List<Restaurant> restaurantList;
//
//		// Find all restaurants
//		restaurantList = restaurantRepository.findAllOrderedByName();
//		Iterator<Restaurant> restaurantIterator = restaurantList.iterator();
//		while (restaurantIterator.hasNext()) {
//			// Get restaurant Id and iterate to next restaurant
//			Integer restid = (int) (long) restaurantIterator.next().getId();
//			// Get votes for restaurant in current election
//			Integer qtyVotes = voteRepository.getVotesByRestaurantIdOfActiveElection(restid);
//			String resName;
//			voteMap.put(restid, qtyVotes);
//
//			// Get name of restaurant to store on vote map view to be rendered
//			resName = restaurantRepository.findById(restid.longValue()).getName();
//			voteMapView.put(restid, qtyVotes);
//
//		}
//		return voteMap;
//	}
//
//	@SuppressWarnings("null")
//	public List<Long> getWinners() throws Exception {
//		try {
//			// Create hashmap of all votes < restaurantId,quantity>
//			Map<Integer, Integer> voteMap = new HashMap<Integer, Integer>();
//			voteMap = createVoteMap();
//			// Get max quantity of votes
//			int maxValueInMap = (Collections.max(voteMap.values()));
//			// create list of most votted restaurants to check for ties
//			List<Long> mostVottedRestaurantIdList = new ArrayList<Long>();
//			// Iterate hash and find restaurants with max votes(check for ties)
//			for (Entry<Integer, Integer> entry : voteMap.entrySet()) {
//				if (entry.getValue() == maxValueInMap) {
//					mostVottedRestaurantIdList.add(entry.getKey().longValue());
//				}
//
//			}
//			// return list with most votted restaurants or most voted restaurant
//			return mostVottedRestaurantIdList;
//		} catch (Exception e) {
//
//		}
//
//		return null;
//	}
}

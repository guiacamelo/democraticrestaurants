package org.jboss.tools.data;

import javax.enterprise.context.ApplicationScoped; 
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import java.util.List;

import org.jboss.tools.model.Restaurant;

@ApplicationScoped
public class RestaurantRepository {

	@Inject
	private EntityManager em;

	public Restaurant findById(Long id) {
		return em.find(Restaurant.class, id);
	}

	public Restaurant findByName(String name) {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Restaurant> criteria = cb.createQuery(Restaurant.class);
		Root<Restaurant> user = criteria.from(Restaurant.class);
		criteria.select(user).where(cb.equal(user.get("name"), name));
		return em.createQuery(criteria).getSingleResult();
	}

	public List<Restaurant> findAllOrderedByName() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Restaurant> criteria = cb.createQuery(Restaurant.class);
		Root<Restaurant> restaurant = criteria.from(Restaurant.class);
		criteria.select(restaurant).orderBy(cb.asc(restaurant.get("name")));
		return em.createQuery(criteria).getResultList();
	}

}

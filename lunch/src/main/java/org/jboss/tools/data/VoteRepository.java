package org.jboss.tools.data;

import javax.enterprise.context.ApplicationScoped; 
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

import org.jboss.tools.model.Vote;

@ApplicationScoped
public class VoteRepository {

	@Inject
	private EntityManager em;

	@Inject
	private ElectionRepository electionRepo;

	public Vote findById(Long id) {
		return em.find(Vote.class, id);
	}

	public int getVotesByRestaurantIdOfActiveElection(long restaurantId) {
		// get current Election
		Long currentElectionId;
		currentElectionId = electionRepo.findActiveElectionOrNull().getId();
		// Create query that return all votes for a restaurant in the active
		// election
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Vote> criteria = cb.createQuery(Vote.class);
		Root<Vote> vote = criteria.from(Vote.class);
		criteria.select(vote);
		criteria.where(cb.equal(vote.get("restaurantId"), restaurantId),
				cb.equal(vote.get("electionId"), currentElectionId));

		List<Vote> voteList;

		voteList = em.createQuery(criteria).getResultList();
		return voteList.size();
	}

	public boolean userCanVote(long userId) {
		List<Vote> voteList;

		voteList = getVotesByUserId(userId);
		if (voteList.size() == 0)
			return true;
		else
			return false;
	}

	public List<Vote> getVotesByUserId(long userId) {
		// get current Election
		Long currentElectionId;
		currentElectionId = electionRepo.findActiveElectionOrNull().getId();

		// Create query that return all votes for a restaurant in the active
		// election
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Vote> criteria = cb.createQuery(Vote.class);
		Root<Vote> vote = criteria.from(Vote.class);
		criteria.select(vote);
		criteria.where(cb.equal(vote.get("electionId"), currentElectionId), cb.equal(vote.get("userId"), userId));
		return em.createQuery(criteria).getResultList();
	}

	public List<Vote> findAllOrderedByElectionId() {
		CriteriaBuilder cb = em.getCriteriaBuilder();
		CriteriaQuery<Vote> criteria = cb.createQuery(Vote.class);
		Root<Vote> vote = criteria.from(Vote.class);
		criteria.select(vote).orderBy(cb.asc(vote.get("id")));
		return em.createQuery(criteria).getResultList();
	}

}

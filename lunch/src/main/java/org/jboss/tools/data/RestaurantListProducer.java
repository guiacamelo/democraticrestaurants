package org.jboss.tools.data;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

import org.jboss.tools.model.Restaurant;

@RequestScoped
public class RestaurantListProducer {

	@Inject
	private RestaurantRepository restaurantRepository;

	private List<Restaurant> restaurants;

	@Produces
	@Named
	public List<Restaurant> getRestaurants() {
		return restaurants;
	}

	public void onRestaurantListChanged(@Observes(notifyObserver = Reception.IF_EXISTS) final Restaurant restaurant) {
		retrieveAllRestaurantsOrderedByName();
	}

	@PostConstruct
	public void retrieveAllRestaurantsOrderedByName() {
		restaurants = restaurantRepository.findAllOrderedByName();
	}
}

package org.jboss.tools.data;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

import org.jboss.tools.model.Vote;

@RequestScoped
public class VoteListProducer {

	@Inject
	private VoteRepository voteRepository;

	private List<Vote> votes;

	@Produces
	@Named
	public List<Vote> getVotes() {
		return votes;
	}

	public void onUserListChanged(@Observes(notifyObserver = Reception.IF_EXISTS) final Vote vote) {
		retrieveAllVotesOrderedByElectionId();
	}

	@PostConstruct
	public void retrieveAllVotesOrderedByElectionId() {
		votes = voteRepository.findAllOrderedByElectionId();
	}
}

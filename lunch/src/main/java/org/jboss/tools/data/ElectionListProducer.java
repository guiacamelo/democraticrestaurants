package org.jboss.tools.data;

import javax.annotation.PostConstruct; 
import javax.enterprise.context.RequestScoped;
import javax.enterprise.event.Observes;
import javax.enterprise.event.Reception;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

import org.jboss.tools.model.Election;

@RequestScoped
public class ElectionListProducer {

	@Inject
	private ElectionRepository electionRepository;

	private List<Election> elections;

	@Produces
	@Named
	public List<Election> getElections() {
		return elections;
	}

	public void onUserListChanged(@Observes(notifyObserver = Reception.IF_EXISTS) final Election election) {
		retrieveAllElectionOrderedByWinner();
	}

	@PostConstruct
	public void retrieveAllElectionOrderedByWinner() {
		elections = electionRepository.findAllOrderedByWinner();
	}
}

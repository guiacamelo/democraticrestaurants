package org.jboss.tools.model;

import java.io.Serializable; 
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@SuppressWarnings("serial")
@Entity
@XmlRootElement
@Table
public class Election implements Serializable {

	@Id
	@GeneratedValue
	private long id;

	private Date voteDate;

	private long winner;

	private boolean active;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public boolean getActiveId() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Date getDate() {
		return voteDate;
	}

	public void setDate(Date lastVoteDate) {
		this.voteDate = lastVoteDate;
	}

	public long getWinner() {
		return winner;
	}

	public void setWinner(long winner) {
		this.winner = winner;
	}
}

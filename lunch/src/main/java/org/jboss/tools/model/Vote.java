package org.jboss.tools.model;

import java.io.Serializable; 

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;


@SuppressWarnings("serial")
@Entity
@XmlRootElement
@Table
public class Vote implements Serializable {

	@Id
	@GeneratedValue
	private Long id;

	private Long electionId;

	private Long restaurantId;
	private Long userId;

	// @ManyToOne
	// @JoinColumn(columnDefinition = "electionId")
	// private Election election;

	public Vote() {

	}

	public Vote(Long electionId, Long restaurantId, Long userId) {
		this.electionId = electionId;
		this.restaurantId = restaurantId;
		this.userId = userId;

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getElectionId() {
		return electionId;
	}

	public void setElectionId(Long electionId) {
		this.electionId = electionId;
	}

	public Long getRestaurantId() {
		return restaurantId;
	}

	public void setRestaurantId(Long restaurantId) {
		this.restaurantId = restaurantId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}

package org.jboss.tools.util;

import java.util.Calendar; 
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class EmailUtils {

	private static final String USERNAME = "democraticlunchdbserver";
	private static final String PASSWORD = "dbserver";
	private static final String FROMEMAIL = "democraticlunchdbserver@gmail.com";

	public static void sendMail(String winner, String name, String email, Date date) {

		final String username = USERNAME;
		final String password = PASSWORD;

		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", "smtp.gmail.com");
		props.put("mail.smtp.port", "587");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {
			Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			int year = cal.get(Calendar.YEAR);
			int month = cal.get(Calendar.MONTH);
			int day = cal.get(Calendar.DAY_OF_MONTH);

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(FROMEMAIL));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
			message.setSubject("Election Result of Day " + day + "/" + month + "/" + year);
			// message.setText(" The Winner Is Restaurant " + winner);
			message.setText("Hi " + name + "\n\n The Winner Is Restaurant " + winner);

			Transport.send(message);

		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

}
package org.jboss.tools.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.inject.Inject;

import org.jboss.tools.data.RestaurantRepository;
import org.jboss.tools.data.VoteRepository;
import org.jboss.tools.model.Restaurant;

@ManagedBean(name = "dtResultView")
@SessionScoped
public class ResultView {
	@Inject
	private VoteRepository voteRepository;
	@Inject
	private RestaurantRepository restaurantRepository;

	List<Pair<String, Integer>> pairList = new ArrayList<Pair<String, Integer>>();

	public List<Pair<String, Integer>> getPairList() {
		// Clear List to get updated values
		pairList.clear();
		createVoteMap();
		return pairList;
	}

	@PostConstruct
	private void createVoteMap() {
		// Get list of all restaurants
		List<Restaurant> restaurantList;
		restaurantList = restaurantRepository.findAllOrderedByName();
		Iterator<Restaurant> restaurantIterator = restaurantList.iterator();
		while (restaurantIterator.hasNext()) {
			// Get restaurant Id and iterate to next restaurant
			Integer restid = (int) (long) restaurantIterator.next().getId();
			Integer qtyVotes = voteRepository.getVotesByRestaurantIdOfActiveElection(restid);
			String resName;

			// Get name of restaurant to store on vote map view to be rendered
			resName = restaurantRepository.findById(restid.longValue()).getName();
			Pair<String, Integer> newPair = new Pair<String, Integer>(resName, qtyVotes);

			pairList.add(newPair);

		}
		return;
	}
}

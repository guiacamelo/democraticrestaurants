package stepDefinition;

import java.sql.SQLException;

import javax.faces.bean.ManagedProperty;

import org.junit.Assert;
import org.junit.runner.RunWith;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.junit.Cucumber;
import cucumber.api.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.*;
import javax.naming.NamingException;


@RunWith(Cucumber.class)
public class UserRegistrationTest {

	

	
//	UserRepository userRepository;

	String name;
	String password;
	String email;
	WebDriver driver;

	
	
	@Given("^a user navigates to register url \"([^\"]*)\"$")
	public void user_navigates_register_url(String url) throws InterruptedException{
		driver = new ChromeDriver();
		driver.get(url);
		Thread.sleep(5000); // Let the user actually see something!		
	}
	
	
	@And("^a user named \"([^\"]*)\" and password \"([^\"]*)\" and email \"([^\"]*)\"$")
	public void user_named_and_password_and_email(String testName, String testPassword, String testEmail)
			throws InterruptedException, SQLException, ClassNotFoundException {
		name = testName;
		password = testPassword;
		email = testEmail;

		// Find the input elements
		WebElement nameInput = driver.findElement(By.id("registerForm:name"));
		WebElement passwordInput = driver.findElement(By.id("registerForm:password"));
		WebElement emailInput = driver.findElement(By.id("registerForm:email"));
		
		// input information to page
		nameInput.sendKeys(name);
		passwordInput.sendKeys(password);
		emailInput.sendKeys(email);
		 
		// WebElement message= driver.findElement(By.name("j_idt7:messages"));
		// System.out.println(message.getText());
		

	}

	@When("^the register function is called$")
	public void register_user_function_call() throws Exception {
		WebElement registerButton = driver.findElement(By.name("registerForm:register"));
		registerButton.click();
		Thread.sleep(5000);
		
		
	}

	@Then("^The login page must be loaded$")
	public void the_user_must_be_in_the_database() throws NamingException, SQLException, ClassNotFoundException {
		// This function is not testing due to a problem found in connecting to the database
		// Could not instanciate an entity Manager since inject don't work for cucumber test
		
		WebElement loginPage=null;
		try{
			
		
		loginPage = driver.findElement(By.name("loginForm:name"));
		
		}catch (Exception e) {
			// TODO: handle exception
			
		}
		
		driver.quit();
		
		//userRepository = new UserRepository();
		//userRepository.findByName(name).getName().contains(name));
		Assert.assertNotNull(loginPage);  
	}

}

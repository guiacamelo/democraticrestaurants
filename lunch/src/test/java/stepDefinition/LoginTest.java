package stepDefinition;

import org.junit.Assert; 
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import org.openqa.selenium.chrome.ChromeDriver;

public class LoginTest {

	String name;
	String password;
	WebDriver driver;



	@Given("^User navigates to login view  \"([^\"]*)\"$")
	public void a_user_navigates_to_the_login_view(String url) throws Throwable {
		driver = new ChromeDriver();
		// Navigate to Login View
		driver.get(url);
		Thread.sleep(5000);

	}


@When("^User enter UserName \"([^\"]*)\" and Password \"([^\"]*)\"$")
public void user_enters_UserName_and_Password(String testName, String testPassword) throws Throwable {
		name = testName;
		password = testPassword;

		WebElement nameInput = driver.findElement(By.id("loginForm:name"));
		WebElement passwordInput = driver.findElement(By.id("loginForm:password"));

		nameInput.sendKeys(name);
		passwordInput.sendKeys(password);

	}


@When("^the login function is called$")
public void login_function_is_called() throws Throwable {
		WebElement loginButton = driver.findElement(By.name("loginForm:login"));
		loginButton.click();
		Thread.sleep(5000);

	}

@Then("^After sucessful login user is Redirected to election view$")
public void after_sucessful_login_user_is_Redirected_to_election_view() throws Throwable {
		WebElement electionPage = null;
		try {
			electionPage = driver.findElement(By.name("electionForm"));

		} catch (Exception e) {
			// TODO: handle exception

		}
		driver.quit();

		Assert.assertNotNull(electionPage);
	}




}

package stepDefinition;
import org.junit.Assert; 
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import org.openqa.selenium.chrome.ChromeDriver;

public class VoteCasting {
	String name;
	String password;
	String successMessage="Vote Recorded!";
	String failMessage="User already casted a vote in this election";
	String restaurantVisitedMessage="Vote Invalid, Restaurant recentely visited!";
	WebDriver driver;
	
	
	@Given("^A user navigates to the login view  \"([^\"]*)\"$")
	public void a_user_navigates_to_the_login_view(String url) throws Throwable {
		driver = new ChromeDriver();
		// Navigate to Login View
		driver.get(url);
		Thread.sleep(5000);
	}

	@When("^User enters UserName \"([^\"]*)\" and Password \"([^\"]*)\"$")
	public void user_enters_UserName_and_Password(String testName, String testPassword) throws Throwable {
		name = testName;
		password = testPassword;

		WebElement nameInput = driver.findElement(By.id("loginForm:name"));
		WebElement passwordInput = driver.findElement(By.id("loginForm:password"));

		nameInput.sendKeys(name);
		passwordInput.sendKeys(password);
	}

	@When("^login function is called$")
	public void login_function_is_called() throws Throwable {
		WebElement loginButton = driver.findElement(By.name("loginForm:login"));
		loginButton.click();
		Thread.sleep(3000);
	}

	@Then("^After sucessful login user is Redirected to election view  \"([^\"]*)\"$")
	public void after_sucessful_login_user_is_Redirected_to_election_view(String arg1) throws Throwable {
		WebElement electionPage = null;
		try {
			electionPage = driver.findElement(By.name("electionForm"));

		} catch (Exception e) {
			// TODO: handle exception

		}
		
		Assert.assertNotNull(electionPage);
	}

	@Then("^The user cast a vote in the first restaurant shown$")
	public void the_user_cast_a_vote() throws Throwable {
		WebElement voteButton = driver.findElement(By.name("electionForm:resVote:0:vote"));
		voteButton.click();
		Thread.sleep(1000);
		WebElement confirmVoteButton = driver.findElement(By.name("electionForm:j_idt21"));
		confirmVoteButton.click();
		Thread.sleep(3000);
	}

	@Then("^The user is redirected to result view \"([^\"]*)\"$")
	public void the_user_is_redirected_to_result_view(String arg1) throws Throwable {
		WebElement resultPage = null;
		try {
			resultPage = driver.findElement(By.name("votemap"));

		} catch (Exception e) {
			// TODO: handle exception

		}
	

		Assert.assertNotNull(resultPage);
	}

	@Then("^The Vote is casted$")
	public void the_Vote_is_casted() throws Throwable {
		String page;
		page =driver.getPageSource();
		
		Assert.assertTrue(page.contains(successMessage));
		
		//Check if message == vote ok
	}

	@Then("^The user clicks the election button and go to election view \"([^\"]*)\"$")
	public void the_user_clicks_the_election_button_and_go_to_election_view(String arg1) throws Throwable {
		WebElement electionButton = driver.findElement(By.name("nav:election"));
		electionButton.click();
		Thread.sleep(3000);
	}

	@Then("^The user cast a vote in the second restaurant shown$")
	public void the_user_cast_a_second_vote() throws Throwable {
		WebElement voteButton = driver.findElement(By.name("electionForm:resVote:1:vote"));
		voteButton.click();
		WebElement confirmVoteButton = driver.findElement(By.name("electionForm:j_idt21"));
		confirmVoteButton.click();
		Thread.sleep(3000);
	}

	@Then("^User is redirected to the result view \"([^\"]*)\"$")
	public void user_is_redirected_to_the_result_view(String arg1) throws Throwable {
		WebElement resultPage = null;
		try {
			resultPage = driver.findElement(By.name("votemap"));

		} catch (Exception e) {
			// TODO: handle exception

		}

		Assert.assertNotNull(resultPage);
		Thread.sleep(3000);
	}
	
	@Then("^The Vote is not casted$")
	public void the_Vote_is_not_casted() throws Throwable {
		String page;
		page =driver.getPageSource();
		
		Assert.assertTrue(page.contains(failMessage));
		
	}



	@Then("^User clicks on finish election$")
	public void user_clicks_on_finish_election() throws Throwable {
		WebElement finishElectionButton = driver.findElement(By.name("nav:finishElection"));
		finishElectionButton.click();
		Thread.sleep(3000);
	}

	@Then("^is redirected to the login view \"([^\"]*)\" and recives the email with the result$")
	public void is_redirected_to_the_login_view_and_recives_the_email_with_the_result(String arg1) throws Throwable {
		WebElement loginPage=null;
		try{
			
			Thread.sleep(17000);
		loginPage = driver.findElement(By.name("loginForm:name"));
		
		}catch (Exception e) {
			// TODO: handle exception
			
		}
		
		//driver.quit();
		
		Assert.assertNotNull(loginPage);  
	}

	@Then("^The Vote is not casted because Restaurant have been recently visited$")
	public void the_Vote_is_not_casted_because_Restaurant_have_been_recently_visited() throws Throwable {
		String page =driver.getPageSource();
		
		Assert.assertTrue(page.contains(restaurantVisitedMessage));
		
	}
	
	@Then("^User Logs Out$")
	public void User_Logs_Out() throws Throwable {
		WebElement finishElectionButton = driver.findElement(By.name("nav:logout"));
		finishElectionButton.click();
		Thread.sleep(3000);
		driver.quit();
	}
	
	
}

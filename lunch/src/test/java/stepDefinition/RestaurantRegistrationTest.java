package stepDefinition;



import javax.faces.bean.ManagedProperty;

import org.junit.Assert;
import org.junit.runner.RunWith;


import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;
import cucumber.api.junit.Cucumber;
import cucumber.api.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.*;

@RunWith(Cucumber.class)
public class RestaurantRegistrationTest {

	

	
//	UserRepository userRepository;

	String name;
	String address;	
	WebDriver driver;
	

	
	
	@Given("^User navigates to register Restaurant view \"([^\"]*)\"$")
	public void user_navigates_to_register_Restaurant_view(String url) throws Throwable {
		driver = new ChromeDriver();
		driver.get(url);
		Thread.sleep(5000); 
	}

	@When("^User enter RestaurantName \"([^\"]*)\" and Adress \"([^\"]*)\"$")
	public void user_enter_RestaurantName_and_Adress(String testName, String testAddress) throws Throwable {
		name = testName;
		address = testAddress;
		
		// Find the input elements
		WebElement nameInput = driver.findElement(By.id("restaurantForm:name"));
		WebElement addressInput = driver.findElement(By.id("restaurantForm:address"));
		
		// input information to page
		nameInput.sendKeys(name);
		addressInput.sendKeys(address);
		
	}

	@When("^the register restaurant function is called$")
	public void the_register_restaurant_function_is_called() throws Throwable {
		WebElement registerButton = driver.findElement(By.name("restaurantForm:register"));
		registerButton.click();
		Thread.sleep(5000);
	}

	@Then("^After the sucessful registration user is Redirected to election view$")
	public void after_the_sucessful_registration_user_is_Redirected_to_election_view() throws Throwable {
		WebElement electionPage=null;
		try{
			
		
			electionPage = driver.findElement(By.name("electionForm"));
		
		}catch (Exception e) {
			// TODO: handle exception
			
		}
		
		driver.quit();
		
		Assert.assertNotNull(electionPage);  
	}

}

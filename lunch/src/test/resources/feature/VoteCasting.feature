#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios 
#<> (placeholder)
#""
## (Comments)

#Sample Feature Definition Template
@VoteCasting
Feature: User Logs in and casts vote
	User logs in and casts vote
 
@VoteCasting1 
Scenario: User Logs in and Casts Vote Sucessfully Once and try to vote again in the same election and fails
Given A user navigates to the login view  "http://localhost:8080/lunch/login.jsf"
When User enters UserName "camelo" and Password "camelo"
And  login function is called
Then After sucessful login user is Redirected to election view  "http://localhost:8080/lunch/election.jsf"
And The user cast a vote in the first restaurant shown
Then The user is redirected to result view "http://localhost:8080/lunch/result.jsf"
And The Vote is casted
Then The user clicks the election button and go to election view "http://localhost:8080/lunch/election.jsf"
And The user cast a vote in the second restaurant shown
Then User is redirected to the result view "http://localhost:8080/lunch/result.jsf"
And The Vote is not casted
Then User clicks on finish election
And is redirected to the login view "http://localhost:8080/lunch/result.jsf" and recives the email with the result
When User enters UserName "bar" and Password "bar"
And  login function is called
Then After sucessful login user is Redirected to election view  "http://localhost:8080/lunch/election.jsf"
And The user cast a vote in the first restaurant shown
Then The user is redirected to result view "http://localhost:8080/lunch/result.jsf"
And The Vote is not casted because Restaurant have been recently visited
Then User Logs Out
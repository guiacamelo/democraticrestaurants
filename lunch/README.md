# Democratic Restaurants: DbServer #

This repository contains the system developed for the technical evaluation of the hiring process at DBServer (Porto Alegre).




**The System**
================================
The Goal of the system is to create a voting platform for the employees of DBServer to decide where they will go to lunch in a democratic way. 
 
Users can register and login into the system. After logged in a User can register a restaurant and cast his vote on where to go have lunch. The user can view the state of the current active election before and after voting. In one election each user is entitled to one vote. No restaurant can be the winner twice in an interval of seven days. The user can Finish an election at any time. At 12:00 if there is an election active, the system will automatically finish the election. Every time an election is finished a winning restaurant is defined according to the votes. If there is a tie in the votes, an arbitrary restaurant will be chosen. Once the winner is chosen an email to all users in the H2 database will be send informing the winner restaurant. The user can logout at any time.



**Approach**
================================

The system have been divided in a MVC Architecture with some adaptations. The controllers are in org.jboss.tools.controller, models are in org.jboss.tools.model, and the views are in /lunch/src/main/webapp/. A Data package have been created to get the data, a Service package have been created to create and update records.




**Repositories**
================================
Repositories have been created to access data from the tables using CriteriaBuilder and CriteriaQuery, and List producers have also been implemented.
They are located in org.jboss.tools.data.




**Registration Service**
================================
A _@Stateless_ class have been created for each table in order to create and update records from the tables using persist of the entities.




**Tables**
================================
The system is divided in 4 tables and each one is described in it's model in package org.jboss.tools.model:

	*  Election Table (Only one election is active at a time)
	*  Restaurant Table
	*  Vote Table
	*  User Table




**REST**
================================
Rest have been used to see the data from the tables in the browser.
It is possible to see all users in:
[ http://localhost:8080/lunch/rest/users ]

It is possible to see all elections in:
[ http://localhost:8080/lunch/rest/elections ]

It is possible to see all votes in:
[ http://localhost:8080/lunch/rest/votes ]

It is possible to see all restaurants in:
[ http://localhost:8080/lunch/rest/restaurants ]
  
  
  
**Technologies**
================================
For the development of this project The following Technologies have been used:

	* Java-SE-1.8
	* JSF 2.0
	* CDI
	* EJB 
	* REST
	* H2 database
	* CriteriaBuilder and CriteriaQuery
	* Primefaces
	* BDD
	* Cucumber
	* Arquilian
	* Selenium
	* Gherkin
	* Maven
	* JBoss
	* ChromeDriver
	* Trello(Kanban)
	

**Logger**
================================
A log was created using  java.util.logging.Logger, the whole system behavior is logged as it executes




**User Session**
================================
A login with session have been implemented with HttpSession, after a valid user tries to log into the system, session keeps userid and username.
An Authorization Filter have been implemented with the intention of blocking pages that should not be accessible before login(Needs Revising)



**BackgroundJobManager - (Timer)**
================================
The class [ /lunch/src/main/java/org/jboss/tools/util/BackgroundJobManager.java ] implements a timer that is executed at noon(12:00) as long as the server is running. It uses @Schedule to run at a specific time.
At that time the election is finished and the winner is decided. The EmailUtils is called send the news to the users

No automated test was created for this function because it would require change the systems time or set the Schedule time at runtime, but this functionality has been manually tested. 

**Note: Ideally This Function Would Just Call Finish election from Election  Controller, but that approach made the timer throw an exception because of FacesContext so for now the approach used repeat some functions from election controller. This should be refactored**
		
		


**EmailUtils**
================================
This Class have been created to send email to all users whenever an election is finished.
An email has been created in gmail to be the sender of the results: democraticlunchdbserver@gmail.com
As it is not of vital information, the email password is inside the in plain text as a string.



**BDD Tests**
================================
  The tests were not made in the ideal way, some work still have to be done in this area, The problem is described in Issues Encountered. 
  The BDD tests performed are described in the features located in [/lunch/src/test/resources/feature] , the step definition and the test execution log are in [/lunch/src/test/java/stepDefinition]
  The Tests were performed using cucumber.
  
  To run the tests, when inside eclipse, select the feature wanted, right click -> Run as ->Cucumber Feature
  Then The chrome browser will open and tthe test will begin.   
  
## User Registration Test ## 
  		\*@UserRegistration
		Feature: User Registration
 		
		@UserRegistration1 
		Scenario: Register User
  		Given a user named "carina" and password "carina" and email "carina@gmail.com" and url "http://localhost:8080/lunch/register.jsf"
		When the register function is called	
		Then The login page must be loaded
		

This is the log of the user registration test:
		@UserRegistration
		Feature: User Registration
		Starting ChromeDriver 2.27.440174 (e97a722caafc2d3a8b807ee115bfb307f7d2cfd9) on port 10873
		Only local connections are allowed.
		
		  @UserRegistration1
		  Scenario: Register User                                                             # C:/Users/Camelo/work/lunch/src/test/resources/feature/UserRegistration.feature:24
		    Given a user navigates to register url "http://localhost:8080/lunch/register.jsf" # UserRegistrationTest.user_navigates_register_url(String)
		    And a user named "carina" and password "carina" and email "carina@gmail.com"      # UserRegistrationTest.user_named_and_password_and_email(String,String,String)
		    When the register function is called                                              # UserRegistrationTest.register_user_function_call()
		    Then The login page must be loaded                                                # UserRegistrationTest.the_user_must_be_in_the_database()
		
		1 Scenarios (1 passed)
		4 Steps (4 passed)
		0m20,922s		
		
## Login Test ##
		@Login
		Feature: Login
			A user types a user name and a password and logs in
		
		@Login1
		Scenario: Sucessful Login  
		Given User navigates to login view  "http://localhost:8080/lunch/login.jsf"
		When User enter UserName "camelo" and Password "camelo"
		And the login function is called
		Then After sucessful login user is Redirected to election view  		
		

This is the log of the login test:
		@Login
		Feature: Login
		  A user types a user name and a password and logs in
		Starting ChromeDriver 2.27.440174 (e97a722caafc2d3a8b807ee115bfb307f7d2cfd9) on port 42898
		Only local connections are allowed.
		
		  @Login1
		  Scenario: Sucessful Login                                                     # C:/Users/Camelo/work/lunch/src/test/resources/feature/Login.feature:25
		    Given User navigates to login view  "http://localhost:8080/lunch/login.jsf" # LoginTest.a_user_navigates_to_the_login_view(String)
		    When User enter UserName "camelo" and Password "camelo"                     # LoginTest.user_enters_UserName_and_Password(String,String)
		    And the login function is called                                            # LoginTest.login_function_is_called()
		    Then After sucessful login user is Redirected to election view              # LoginTest.after_sucessful_login_user_is_Redirected_to_election_view()
		
		1 Scenarios (1 passed)
		4 Steps (4 passed)
		0m20,189s
  		
  
## Restaurant Registration ##
		@RegisterRestaurant
		Feature: Register Restaurant
			Restaurant is registered 
		
		@RegisterRestaurant1
		Scenario: Sucessful Register Restaurant
		Given User navigates to register Restaurant view "http://localhost:8080/lunch/restaurant.jsf"
		When User enter RestaurantName "Fragata" and Adress "Assis Brasil"
		And the register restaurant function is called
		Then After the sucessful registration user is Redirected to election view
		
This is the log of the Restaurant Registration test:

		  @RegisterRestaurant1
		  Scenario: Sucessful Register Restaurant                                                         # C:/Users/Camelo/work/lunch/src/test/resources/feature/RestaurantRegistration.feature:25
		    Given User navigates to register Restaurant view "http://localhost:8080/lunch/restaurant.jsf" # RestaurantRegistrationTest.user_navigates_to_register_Restaurant_view(String)
		    When User enter RestaurantName "Fragata" and Adress "Assis Brasil"                            # RestaurantRegistrationTest.user_enter_RestaurantName_and_Adress(String,String)
		    And the register restaurant function is called                                                # RestaurantRegistrationTest.the_register_restaurant_function_is_called()
		    Then After the sucessful registration user is Redirected to election view                     # RestaurantRegistrationTest.after_the_sucessful_registration_user_is_Redirected_to_election_view()
		
		1 Scenarios (1 passed)
		4 Steps (4 passed)
		0m19,809s
  
  
## Vote Casting And Other Functionalities##
This test cover some situations regarding the system requirements. If the Entity Manager where accessible in the tests this test would be different, and tests in smaller cases.
Functionalities covered: 

+ Login 
+ Vote for a restaurant in a election
+ Try to vote for a second election and fail
+ Finish Election(winner Restaurant is elected)
+ Login of different user
+ Try to vote for winning restaurant but fails because restaurant has won an election in the past 7 days
+ Logout


Vote Casting And Other Functionalities Feature:

	@VoteCasting
	Feature: User Logs in and casts vote
		User logs in and casts vote
	 
	@VoteCasting1 
	Scenario: User Logs in and Casts Vote Sucessfully Once and try to vote again in the same election and fails
	Given A user navigates to the login view  "http://localhost:8080/lunch/login.jsf"
	When User enters UserName "camelo" and Password "camelo"
	And  login function is called
	Then After sucessful login user is Redirected to election view  "http://localhost:8080/lunch/election.jsf"
	And The user cast a vote in the first restaurant shown
	Then The user is redirected to result view "http://localhost:8080/lunch/result.jsf"
	And The Vote is casted
	Then The user clicks the election button and go to election view "http://localhost:8080/lunch/election.jsf"
	And The user cast a vote in the second restaurant shown
	Then User is redirected to the result view "http://localhost:8080/lunch/result.jsf"
	And The Vote is not casted
	Then User clicks on finish election
	And is redirected to the login view "http://localhost:8080/lunch/result.jsf" and recives the email with the result
	When User enters UserName "bar" and Password "bar"
	And  login function is called
	Then After sucessful login user is Redirected to election view  "http://localhost:8080/lunch/election.jsf"
	And The user cast a vote in the first restaurant shown
	Then The user is redirected to result view "http://localhost:8080/lunch/result.jsf"
	And The Vote is not casted because Restaurant have been recently visited
	Then User Logs Out	
	 
This is the log of the Vote Casting And Other Functionalities:
		@VoteCasting
		Feature: User Logs in and casts vote
		  User logs in and casts vote
		Starting ChromeDriver 2.27.440174 (e97a722caafc2d3a8b807ee115bfb307f7d2cfd9) on port 47079
		Only local connections are allowed.
		
		  @VoteCasting1
		  Scenario: User Logs in and Casts Vote Sucessfully Once and try to vote again in the same election and fails          # C:/Users/Camelo/work/lunch/src/test/resources/feature/VoteCasting.feature:25
		    Given A user navigates to the login view  "http://localhost:8080/lunch/login.jsf"                                  # VoteCasting.a_user_navigates_to_the_login_view(String)
		    When User enters UserName "camelo" and Password "camelo"                                                           # VoteCasting.user_enters_UserName_and_Password(String,String)
		    And login function is called                                                                                       # VoteCasting.login_function_is_called()
		    Then After sucessful login user is Redirected to election view  "http://localhost:8080/lunch/election.jsf"         # VoteCasting.after_sucessful_login_user_is_Redirected_to_election_view(String)
		    And The user cast a vote in the first restaurant shown                                                             # VoteCasting.the_user_cast_a_vote()
		    Then The user is redirected to result view "http://localhost:8080/lunch/result.jsf"                                # VoteCasting.the_user_is_redirected_to_result_view(String)
		    And The Vote is casted                                                                                             # VoteCasting.the_Vote_is_casted()
		    Then The user clicks the election button and go to election view "http://localhost:8080/lunch/election.jsf"        # VoteCasting.the_user_clicks_the_election_button_and_go_to_election_view(String)
		    And The user cast a vote in the second restaurant shown                                                            # VoteCasting.the_user_cast_a_second_vote()
		    Then User is redirected to the result view "http://localhost:8080/lunch/result.jsf"                                # VoteCasting.user_is_redirected_to_the_result_view(String)
		    And The Vote is not casted                                                                                         # VoteCasting.the_Vote_is_not_casted()
		    Then User clicks on finish election                                                                                # VoteCasting.user_clicks_on_finish_election()
		    And is redirected to the login view "http://localhost:8080/lunch/result.jsf" and recives the email with the result # VoteCasting.is_redirected_to_the_login_view_and_recives_the_email_with_the_result(String)
		    When User enters UserName "bar" and Password "bar"                                                                 # VoteCasting.user_enters_UserName_and_Password(String,String)
		    And login function is called                                                                                       # VoteCasting.login_function_is_called()
		    Then After sucessful login user is Redirected to election view  "http://localhost:8080/lunch/election.jsf"         # VoteCasting.after_sucessful_login_user_is_Redirected_to_election_view(String)
		    And The user cast a vote in the first restaurant shown                                                             # VoteCasting.the_user_cast_a_vote()
		    Then The user is redirected to result view "http://localhost:8080/lunch/result.jsf"                                # VoteCasting.the_user_is_redirected_to_result_view(String)
		    And The Vote is not casted because Restaurant have been recently visited                                           # VoteCasting.the_Vote_is_not_casted_because_Restaurant_have_been_recently_visited()
		    Then User Logs Out                                                                                                 # VoteCasting.User_Logs_Out()
		
		1 Scenarios (1 passed)
		20 Steps (20 passed)
		0m57,680s



**Note: When running the tests the Chrome drive must be set to the location installed in the local computer with the command below set in the init function of every test that involves Webdriver**
	System.setProperty("webdriver.chrome.driver", <path/to/chrome/web/driver.exe>);
	

**Issues Encountered**
================================

  The main problem with the tests was that it was not possible to get an instance of the EntityManager when running the BDD Tests since when running cucumber @Inject does not work()    
  
  One Solution for that problem that would require a design change would be to use Spring and Mockit so it would be possible to access an Entity Manager instance and persist data against the data base. 
  
  Other solution would be to create an instance of EntityManagerFactory and EntityManager using PersistenceContext(unitName ="primary")           

**Possible Improvements**
================================
	
The exception handling can be improved.
The Tests are not ideal because the Entity Manager could not be used in the cucumber tests. 


# Instructions to execute  #

Here are the instruction to build, and run the system.

** Installing Required Programs **
================================

** JDK 1.8**
   You will need JDK 1.8
   www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html
   After installing JDK go to Eclipse Window-> Preferences->Java-> Installed JRE
   Click add and insert the path to JDK installation
   After That Add JDK installation path to you PATH variable


** Eclipse**
   First of all you will need an eclipse installation. I used Eclipse Neon
   https://www.eclipse.org/downloads/


** Installing Jboss EAP-7.0.0**
   Download and install jboss 
   https://developers.redhat.com/products/eap/download/
   You will need to know the installation Path 

  
** Installing Cucumber in Eclipse**
   Install Cucumber following instruction from:
   https://www.linkedin.com/pulse/cucumber-selenium-webdriver-java-maven-prashanth-aedulapuram
   
   In Eclipse:
	Click on Help --> Install New Software
	In the Search field, search for url http://cucumber.github.com/cucumber-eclipse/update-site
	and click on Enter. Select the checkbox Cucumber Eclipse Plugin and install
   
  
** Installing JBoss Tools 4.4.3**
   Go to eclipse Market and install JBoss Tools 4.4.3

** Installing Chrome Driver**
	Download Chrome Driver from : https://sites.google.com/a/chromium.org/chromedriver/getting-started
	It is an executable, so there is no need to install it, just unzip it in the directory you want and add that directory to the Environmental Variable PATH

** Setting up the Environment **
================================


** Download the project From bitbucket**
	Open Eclipse, then File-> Import-> Git -> Projects from Git
  Add the git repository, and download the project
  git@bitbucket.org:guiacamelo/democraticrestaurants.git 
  
** Update Maven dependencies**
  Once the project is downloaded,  right click in project->Maven -> Update Project ...
  Maven will download all dependencies described in pom.xml  
  
 At that point, eclipse might find an error in pom.xml regarding those libraries:
 	
	* jboss-jsp-api_2.3_spec-1.0.1.Final-redhat-1.jar
	* jboss-el-api_3.0_spec-1.0.6.Final-redhat-1.jar
	* jboss-servlet-api_3.1_spec-1.0.0.Final-redhat-1.jar
	* async-http-servlet-3.0-3.0.16.Final-redhat-1.jar
	* artemis-journal-1.1.0.SP16-redhat-1.jar
	* javax.mail-1.5.5.redhat-1.jar
	
Eclipse will also suggest a fix that will solve the issue. Performe the suggested Fix "Configure The Red Hat Maven Repository".

The Fix will create a configuration file settings.xml ( for me it created in c:Users\Camelo\.m2\settings.xml)
   This is the file:
	   
	<?xml version="1.0" encoding="UTF-8" standalone="no"?>
	<settings xmlns="http://maven.apache.org/SETTINGS/1.1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd">
	  <profiles>
	    <profile>
	      <id>redhat-ga-repository</id>
	      <repositories>
	        <repository>
	          <id>redhat-ga-repository</id>
	          <name>Red Hat GA repository</name>
	          <url>http://maven.repository.redhat.com/ga/</url>
	          <releases>
	            <enabled>true</enabled>
	            <updatePolicy>never</updatePolicy>
	          </releases>
	          <snapshots>
	            <enabled>false</enabled>
	            <updatePolicy>daily</updatePolicy>
	          </snapshots>
	        </repository>
	      </repositories>
	      <pluginRepositories>
	        <pluginRepository>
	          <id>redhat-ga-repository</id>
	          <name>Red Hat GA repository</name>
	          <url>http://maven.repository.redhat.com/ga/</url>
	          <releases>
	            <enabled>true</enabled>
	            <updatePolicy>never</updatePolicy>
	          </releases>
	          <snapshots>
	            <enabled>false</enabled>
	            <updatePolicy>daily</updatePolicy>
	          </snapshots>
	        </pluginRepository>
	      </pluginRepositories>
	    </profile>
	  </profiles>
	  <activeProfiles>
	    <activeProfile>redhat-ga-repository</activeProfile>
	  </activeProfiles>
	</settings>
	    
Update maven again and check the checkbox "Force Update of Snapshot/Release"

	    
	    
** Create a Jboss Server **
  Inside eclipse in the server tab click add new server and select JBoss 7.1
  In Home Directory input the EAP-7.0.0 installation directory. In Execution environment chose javaSE-1.8
  You can test if Jboss is working in address:
  http://localhost:8080/
  
  
** Building the project and Running on the Server **
  Open the project in eclipse, right click on project , Maven-> update project

  After update finished, right click on project, Run As-> Maven Install
  
  After sucessfull installation
  
  Create/Start the server  
  Click on the created server and Start
  
  Right Click on Project, Run as..-> Run on Server
 
	
	
** Troubleshooting **
  - I had the following problems, Using jdk1.8 in pom.xml made some problems occur in some maven items, so I had to use 1.7
  - JDK installation was not indexed nor was added to the PATH environment variable. Solved following  JDK 1.8
  - I had the problem with the JBOSS libraries but the fix for that was suggested by eclipse
  - If the project still presents errors it is worth to try the following:
 
	* Close Eclipse 
	* Go to the Jboss EAP-7.0.0, delete all content inside EAP-7.0.0\standalone\deployments, and EAP-7.0.0\standalone\tmp. 
	* Then go to the directory where the eclipse binary is and open a command prompt there(shift + right mouse click) 
	* Open eclipse from the prompt with the -clean flag: eclipse -clean
	
	
  - If Problem with the server is presented delete the server and then create a identical server.
  
  - Some libraries might get corrupted when downloading with maven, one possible fix is to check the box "Force Update of Snapshop/Release" When doing the Maven --> UpdateProject  
   
   
- If errors with message similar  "invalid LOC header (bad signature)" you can follow the instructions in this stackoverflow thread.
 [http://stackoverflow.com/questions/23310474/hibernate-commons-annotations-4-0-1-final-jar-invalid-loc-header-bad-signature]

Close Eclipse, go to your documents... /.m2/repository and delete all content or just the content that is causing the problem. 
After that open run mvn clean install. This should fix the problem
		